#include <stdio.h>
#include <stdlib.h>

// cuda header
#include <cuda.h>

__device__ void func1() {} 	

__global__ void kernel(int* d_a) {
	int idx = threadIdx.x + blockIdx.x * blockDim.x;

	printf("hello world from device: kernel of idx = %d\n", idx);

	d_a[idx] += 1;

	// simple call
	func1();
}

int main(int args, char* argv[]) {
	
	if(args < 2) {
		fprintf(stderr, "Failed.\n\nUsage: $ ./a.out <size>\n\ne.g: $ ./a.out 9\n\n");
		exit(1); // safe quit
	}

	int size = atoi(argv[1]);

	// var init
	int *h_a; // host data
	int *d_a; // device data

	h_a = (int*) calloc(size, sizeof(int)); // initialize dynamic array 

	// put dummy data
	int i; 
	for(i = 0; i < size; i++) {
		h_a[i] = i;
	}

	// display dummy data
	for(i = 0; i < size; i++) {
		printf("h_ai[%d] = %d\n", i, h_a[i]);
	}

	// mem alloc
	size_t cudaSize = size * sizeof(int);
	cudaMalloc((void**) &d_a, cudaSize);

	// cuda memcopy host-to-device
	cudaMemcpy(d_a, h_a, cudaSize, cudaMemcpyHostToDevice);

	// kernel launches
	kernel<<< 1, 4 >>>(d_a);

	// cuda memcopy device-to-host
	cudaMemcpy(h_a, d_a, cudaSize, cudaMemcpyDeviceToHost);

	// display dummy data
	for(i = 0; i < size; i++) {
		printf("h_a[%d] = %d\n", i, h_a[i]);
	}

	// cleanup
	cudaFree(d_a);


	return 0;
}
