CUDA Skeleton
=============
A simple cuda skeleton.

author
------
 * hadrihl // hadrihilmi@gmail.com

How-to Compile
--------------
$ nvcc cuda.cu -arch=sm_20 -G -g -o a.out